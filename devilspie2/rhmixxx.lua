if (get_application_name()=="Mixxx 1.11.0") then
   unmaximize()
   set_viewport(2)
   set_window_fullscreen(true)
   pin_window()
else
   set_viewport(1)
end
