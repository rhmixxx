//***********************************************************
//**      Radio Helsinki RHMixxx control script            **
//**    CopyLeft 2014, Josef Schauer / Christian Pointner  **
//***********************************************************

// *********** Preamble  ***********

function RHMixxx() {}

RHMixxx.groupNotes = {
    "[Channel1]": { play: 0x01, stop: 0x00 },
    "[Channel2]": { play: 0x03, stop: 0x02 },
    "[Sampler1]": 0x04,
    "[Sampler2]": 0x05,
    "[Sampler3]": 0x06,
    "[Sampler4]": 0x07,
    "[Sampler5]": 0x08,
    "[Sampler6]": 0x09,
    "[Sampler7]": 0x0A,
    "[Sampler8]": 0x0B,
    "[Sampler9]": 0x0C,
    "[Sampler10]": 0x0D,
    "[Sampler11]": 0x0E,
    "[Sampler12]": 0x0F
}

// *********** Init/De-Init  ***********

RHMixxx.init = function (id, debug) {
    var num_decks = engine.getValue("[Master]", "num_decks");
    var num_sampler = engine.getValue("[Master]", "num_samplers");

    if(debug)
        print("RHMixxx initializing for controller '" + id +
              "' (" + num_decks + " Decks, " + num_sampler + " Sampler)");

    var i;
    for (i = 1; i <= num_decks; i++)
        RHMixxx.initDeck(i, debug);
    for (i = 1; i <= num_sampler; i++)
        RHMixxx.initSampler(i, debug);

    midi.sendShortMsg(0x80, 0x1F, 0x00);
}

RHMixxx.shutdown = function () {
}


RHMixxx.initDeck = function (c, debug) {
    var group = "[Channel" + c + "]";

    engine.setValue(group, "volume", 0);
    engine.connectControl(group, "play", "RHMixxx.DeckPlaying");
    engine.trigger(group, "play");
    engine.connectControl(group, "track_samples", "RHMixxx.DeckSamples");
    engine.trigger(group, "track_samples");

    if(debug)
        print("RHMixxx: " + group + " initilized!");
}

RHMixxx.initSampler = function (s, debug) {
    var group = "[Sampler" + s + "]";

    engine.setValue(group, "volume", 1.0);
    engine.connectControl(group, "play", "RHMixxx.SamplerPlaying");
    engine.trigger(group, "play");
    engine.connectControl(group, "track_samples", "RHMixxx.SamplerSamples");
    engine.trigger(group, "track_samples");

    if(debug)
        print("RHMixxx: " + group + " initilized!");
}

// *********** Actions  ***********

RHMixxx.StopDeck = function (channel, control, value, status, group) {
    engine.setValue(group, "start_stop", value);
}

RHMixxx.ToggleSampler = function (channel, control, value, status, group) {
    print("RHMixxx: ToggleSampler called for " + group);

    if(!engine.getValue(group, "play")) {
        if(value)
            engine.setValue(group, "play", 1);
    }
    else {
        engine.setValue(group, "start_stop", value);
    }
}


// *********** Status updates  ***********

RHMixxx.DeckPlaying = function (value, group, control) {
    if (!value) {
        if(engine.getValue(group, "track_samples") != 0) {
           midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].stop, 0x00);
           midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].play, 0x00);
        } else {
           midi.sendShortMsg(0x80, RHMixxx.groupNotes[group].stop, 0x00);
           midi.sendShortMsg(0x80, RHMixxx.groupNotes[group].play, 0x00);
        }
    } else {
        midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].stop, 0x00);
        midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].play, 0x10);
    }
}

RHMixxx.DeckSamples = function (value, group, control) {
    if(!value) {
        midi.sendShortMsg(0x80, RHMixxx.groupNotes[group].stop, 0x00);
        midi.sendShortMsg(0x80, RHMixxx.groupNotes[group].play, 0x00);
    } else {
        if(engine.getValue(group, "play") == 0) {
            midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].stop, 0x00);
            midi.sendShortMsg(0x90, RHMixxx.groupNotes[group].play, 0x00);
        }
    }
}

RHMixxx.SamplerPlaying = function (value, group, control) {
    if (!value) {
        if(engine.getValue(group, "track_samples") != 0) {
            midi.sendShortMsg(0x90, RHMixxx.groupNotes[group], 0x00);
            engine.setValue(group, "start_stop", 1);
            engine.setValue(group, "start_stop", 0);
        } else {
            midi.sendShortMsg(0x80, RHMixxx.groupNotes[group], 0x00);
        }
    }
    else {
        midi.sendShortMsg(0x90, RHMixxx.groupNotes[group], 0x10);
    }
}

RHMixxx.SamplerSamples = function (value, group, control) {
    if(!value) {
        midi.sendShortMsg(0x80, RHMixxx.groupNotes[group], 0x00);
    } else {
        if(engine.getValue(group, "play") == 0)
            midi.sendShortMsg(0x90, RHMixxx.groupNotes[group], 0x00);
    }
}
